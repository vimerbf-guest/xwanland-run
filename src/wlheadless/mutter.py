#
# Copyright © 2023 Red Hat, Inc
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.
#
#   Olivier Fourdan <ofourdan@redhat.com>
#

""" Abstraction for running a Wayland client on mutter headless. """

from os import environ, getpid
from wlheadless.wlheadless_common import WlheadlessCommon
from wlheadless.xwayland import Xwayland

class Wlheadless:

    """
    Abstraction for running a Wayland client on mutter headless.
    """

    def __call__(self):
        return self


    def __init__(self):
        self.compositor_args = []
        self.compositor = [
            'dbus-run-session',
            'mutter',
            '--wayland',
            '--headless',
        ]
        self.wlheadless_common = WlheadlessCommon()
        self.xwayland = Xwayland()
        self.options = self.wlheadless_common.get_command_output(['mutter', '-h'])


    def spawn_client(self, command_args):
        """Helper function to spawn mutter and the client at once."""
        wayland_display = 'wayland-' + format(getpid())
        environ['WAYLAND_DISPLAY'] = wayland_display
        compositor = self.compositor
        # The option '--no-x11' might not be supported if mutter was built
        # without X11 support.
        if '--no-x11' in self.options:
            compositor.extend(['--no-x11'])
        compositor.extend(self.compositor_args)
        compositor.extend(['--wayland-display', format(wayland_display)])
        compositor.extend(['--'])
        compositor.extend(command_args)
        return self.wlheadless_common.run_command(compositor)


    def spawn_xwayland(self, xserver_args = []):
        """Helper function to spawn mutter and Xwayland at once."""
        compositor = self.compositor
        if '--no-x11' in self.options:
            compositor.extend(['--no-x11'])
        compositor.extend(self.compositor_args)
        compositor.extend(['--'])
        xserver_args.extend(['-fullscreen'])
        return self.xwayland.spawn_xwayland(xserver_args, compositor)


    def wait_compositor(self):
        """Waits for the compositor to start."""
        return 0


    def run_compositor(self, compositor_args = []):
        """Starts the Wayland compositor."""
        # Add at least one virtual monitor if none is specified
        if not '--virtual-monitor' in compositor_args:
            compositor_args.extend(['--virtual-monitor', '1024x768'])
        # Just save the given args for when we shall actually spawn the compositor.
        self.compositor_args = compositor_args
        return 0
