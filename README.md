xwayland-run
============

`xwayland-run` contains a set of small utilities revolving around running `Xwayland`
and various Wayland compositor headless.

Initially, `xwayland-run` was intended as a replacement for the `xvfb-run` utility
that ships with some distributions.

However, while implementing the features for spawning various Wayland compositors
headless and `Xwayland` rootful, it became clear that it might be useful to
actually split that into different (yet related) smaller utilities which could be
used separately for different purposes.

So now, `xwayland-run` actually contains 3 different tools, namely:

 * `xwayland-run`, to spawn an X11 client within its own dedicated `Xwayland`
   rootful instance,
 * `wlheadless-run` to run a Wayland client on a set of supported Wayland
   headless compositors,
 * and `xwfb-run`, a combination of the two other tools above to be used as a
   direct replacement for `xvfb-run` specifically.

Both `wlheadless-run` and `xwfb-run` can use various headless Wayland compositors,
and currently support the following compositors:

 * [weston](https://gitlab.freedesktop.org/wayland/weston)
 * [cage](https://github.com/cage-kiosk/cage)
 * [kwin](https://invent.kde.org/plasma/kwin)
 * [gnome-kiosk](https://gitlab.gnome.org/GNOME/gnome-kiosk)
 * [mutter](https://gitlab.gnome.org/GNOME/mutter)

It should be possible to add more compositors to this list by creating additional
modules for supporting those.

`xwayland-run`
--------------

`xwayland-run` is meant to be a helper utility to run an X11 client within a
dedicated `Xwayland` rootful server.

It can be used to spawn legacy X11 games for example or any other X11 application
that would benefit from running in `Xwayland` rootful.

It takes care of starting `Xwayland`, setting the X11 `DISPLAY` environment variable,
setting up `xauth` and running the specified X11 client using the newly started
`Xwayland` instance.

When the X11 client terminates, `xwayland-run` will automatically close the
dedicated `Xwayland` server.

```
usage: xwayland-run <Xwayland arguments> -- xclient <X client arguments>
```

The given `Xwayland` arguments must be supported by `Xwayland` and will be added
to the `Xwayland` command line.

Example:

```sh
$ xwayland-run -fullscreen -host-grab -- wine wingame.exe
```

```sh
$ xwayland-run -decorate -geometry 800x600 -- glxgears -fullscreen
```

`wlheadless-run`
----------------

As its name implies, `wlheadless-run` is meant to run a Wayland client on a Wayland
compositor running headless.

Every Wayland compositor comes with its own command line options and syntax for
running headless, `wlheadless-run` is an attempt at hiding the differences between
compositors and providing a common way to launch a Wayland client within a dedicated
Wayland compositor.

```
usage: wlheadless-run [-c compositor] <compositor arguments> -- client <client arguments>
```

The compositor must be supported by `wlheadless-run`, which currently includes
the following compositors: `weston`, `cage`, `kwin`, `mutter`, `gnome-kiosk`.

The given compositor arguments must be supported by the specified Wayland compositor
and will be added to the command line when starting the compositor. That allows for
additional compositor options to be specified.

Example:

```sh
$ wlheadless-run -c weston --renderer gl -- weston-simple-dmabuf-feedback

feedback: main device /dev/dri/renderD128
├─── tranche: target device /dev/dri/renderD128, no flags
[…]
```

```sh
% wlheadless-run -c mutter -- wayland-info -i wp_fractional_scale_manager_v1 2>/dev/null

interface: 'wp_fractional_scale_manager_v1',             version:  1, name: 11
```

`xwfb-run`
----------

`xwfb-run` is intended as a direct replacement for `xvfb-run`.

It allows to run X11 clients on a dedicated `Xwayland` server running fullscreen
on a headless Wayland compositor.

```
usage: xwfb-run [-h] [-a AUTO_SERVERNUM] [-c COMPOSITOR] [-d AUTO_DISPLAY]
                [-e ERROR_FILE] [-f AUTH_FILE] [-n SERVER_NUM] [-l] [-p XAUTH_PROTO]
                [-p XAUTH_PROTO] [-s SERVER_ARGS] [-w WAIT]
                -- command ...

Run COMMAND (usually an X client) in a virtual X server environment.

positional arguments:
  command               The program to run and arguments.

options:
    -h, --help          show this help message and exit
  -a AUTO_SERVERNUM, --auto-servernum AUTO_SERVERNUM
                        Unused, kept for backward compatibility only.
  -c COMPOSITOR, --compositor COMPOSITOR
                        Use the compositor class implementation.
  -d AUTO_DISPLAY, --auto-display AUTO_DISPLAY
                        Unused, kept for backward compatibility only.
  -e ERROR_FILE, --error-file ERROR_FILE
                        File used to store xauth and X server errors.
  -f AUTH_FILE, --auth-file AUTH_FILE
                        File used to store auth cookie.
  -n SERVER_NUM, --server-num SERVER_NUM
                        Xserver number to use.
  -l, --listen-tcp      Enable TCP port listening in the X server.
  -p XAUTH_PROTO, --xauth-proto XAUTH_PROTO
                        Xauthority protocol name to use.
  -s SERVER_ARGS, --server-args SERVER_ARGS
                        Arguments to pass to the X server.
  -w WAIT, --wait WAIT  Delay in seconds to wait for the X server to start before running COMMAND.
  -z COMPOSITOR_ARGS, --compositor-args COMPOSITOR_ARGS
                        Arguments to pass to the Wayland compositor.
```

The compositor must be supported by `xwfb-run`, which currently includes the
following compositors: `weston`, `cage`, `kwin`, `mutter`, `gnome-kiosk`.

Optional arguments passed to the Xserver (using the `-s` or `--server-args` option)
must be escaped twice to prevent `xwfb-run` from trying to parse them, e.g. `-s \\-ac`

Arguments intended to the Wayland compositor (using the `-z` or `--compositor-args`
option) must also be escaped with two backslash characters, e.g. `-z \\--renderer -z gl`.

Example:

```sh
$ xwfb-run -c weston -z \\--renderer -z gl -- glxinfo -B

name of display: :2
display: :2  screen: 0
direct rendering: Yes
Extended renderer info (GLX_MESA_query_renderer):
    Vendor: Intel (0x8086)
    Device: Mesa Intel(R) UHD Graphics 620 (KBL GT2) (0x5917)
    Version: 23.2.1
    Accelerated: yes
    Video memory: 15369MB
    Unified memory: yes
    Preferred profile: core (0x1)
    Max core profile version: 4.6
    Max compat profile version: 4.6
    Max GLES1 profile version: 1.1
    Max GLES[23] profile version: 3.2
OpenGL vendor string: Intel
OpenGL renderer string: Mesa Intel(R) UHD Graphics 620 (KBL GT2)
OpenGL core profile version string: 4.6 (Core Profile) Mesa 23.2.1
OpenGL core profile shading language version string: 4.60
OpenGL core profile context flags: (none)
OpenGL core profile profile mask: core profile

OpenGL version string: 4.6 (Compatibility Profile) Mesa 23.2.1
OpenGL shading language version string: 4.60
OpenGL context flags: (none)
OpenGL profile mask: compatibility profile

OpenGL ES profile version string: OpenGL ES 3.2 Mesa 23.2.1
OpenGL ES profile shading language version string: OpenGL ES GLSL ES 3.20
```

```sh
$ xwfb-run -c weston -s \\-geometry -s 2048x1536 -- xrandr -q

Screen 0: minimum 16 x 16, current 2048 x 1536, maximum 32767 x 32767
XWAYLAND0 connected 2048x1536+0+0 541mm x 406mm
   5120x2880     59.99 +
   4096x2304     59.99
   3840x2160     59.98
   3200x1800     59.96
   2880x1620     59.96
   2560x1600     59.99
   2560x1440     59.96
   2048x1536     59.95*
[…]
```

Configuration
-------------

The default compositor can be configured using the `wlheadless.conf` file:

```
[DEFAULT]
    Compositor = weston
```

The file is searched in `wlheadless/` relative to the paths specified in the
standard environment variables `XDG_DATA_HOME` and `XDG_DATA_DIRS`.

Building
--------

`xwayland-run` uses the [Meson build system](https://mesonbuild.com/),
building is just a matter of running within the source directory:

```
$ meson setup -Dcompositor=weston . build
$ meson install -C build
```
